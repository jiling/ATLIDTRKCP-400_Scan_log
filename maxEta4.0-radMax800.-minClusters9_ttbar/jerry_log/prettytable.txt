┌───────────────┬────────────────────┬────────────────────┐
│ event CPU (s) │ ITkSiSpTrackFinder │ ITkAmbiguitySolver │
│        String │            Float64 │            Float64 │
├───────────────┼────────────────────┼────────────────────┤
│      Standard │            4.06952 │            4.95036 │
│       LargeD0 │            8.16657 │           0.228685 │
├───────────────┼────────────────────┼────────────────────┤
│      Slowdown │                2.0 │                0.0 │
└───────────────┴────────────────────┴────────────────────┘
